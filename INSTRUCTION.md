**Pembuatan Halaman Baru**
- buat fungsi baru di views.py
bentuk biasa seperti berikut
```
def func_name(request):
    return render(request, 'nama_file_html.html')
```
- tambahkan value ke dalam variable urlpatterns di file urls.py
```
urlpatterns = [
    ...,
    path('link/menuju/halaman/', views.func_name, name='nama-halaman'),
]
```
- buat file html di folder templates
- selesai, buka halaman dengan url `localhost:8000/link/menuju/halaman` untuk melihat hasil

**Menjalankan SQL Query**

Dalam file views.py, untuk menjalankan query
```
cursor.execute('select * from pelanggan')
```

Untuk menyimpan hasil query ke variabel result untuk diakses nantinya
```
result = cursor.execute('select * from pelanggan')
```

Untuk menjalankan query dengan variabel gunakan '%s'
```
cursor.execute('select * from pelanggan where id_pelanggan = %s', id_pelanggan)
```
