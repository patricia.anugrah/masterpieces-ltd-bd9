from django.shortcuts import render, reverse, redirect
from .quering import QueryExecuter
from .forms import LoginForm

cursor = QueryExecuter()

def loggedin(view_func):
    def wrapper(request, *args, **kwargs):
        if request.session.get('pelanggan_loggedin', False):
            return view_func(request, *args, **kwargs)
        return redirect('peminjaman-login')
    return wrapper

def loggedout(view_func):
    def wrapper(request, *args, **kwargs):
        if not request.session.get('pelanggan_loggedin', False):
            return view_func(request, *args, **kwargs)
        return redirect('base')
    return wrapper

# Create your views here.

def base(request):
    context = {}

    context['request'] = request
    return render(request, 'base.html', context)

@loggedout
def peminjaman_login(request):
    context = {
        'form' : LoginForm()
    }

    if request.method == 'POST':
        post_form = LoginForm(request.POST)
        if post_form.is_valid():
            id_pelanggan = post_form.cleaned_data.get('id_pelanggan')
            email = post_form.cleaned_data.get('email_pelanggan')
            result = cursor.execute('select * from staff where no_staff = %s and email = %s', id_pelanggan, email, return_type = 'dict')
            if len(result) == 1:
                pelanggan = result[0]
                request.session['pelanggan'] = {i : str(j) for i,j in pelanggan.items()}
                request.session['pelanggan_loggedin'] = True
                return redirect('pengguna-penyewa')
            elif len(result) > 1:
                context['error'] = 'Terdapat lebih dari satu staff dengan nomor dan email yang sama'
            else:
                context['error'] = 'Tidak ditemukan staff dengan nomor dan email yang diberikan'
            context['form'] = post_form
        else:
            context['error'] = 'Input tidak valid'
            context['form'] = post_form

    context['request'] = request
    return render(request, 'login.html', context)

def peminjaman_logout(request):
    request.session['pelanggan'] = None
    request.session['pelanggan_loggedin'] = False
    return redirect('pengguna-penyewa')

def pengguna_penyewa(request):
    context = {}

    result = cursor.execute('select * from pelanggan p join penyewa pw on p.id_pelanggan = pw.id_pelanggan', return_type = 'dict')
    for i in range(len(result)):
        current = result[i]
        result[i] = {j : k for j,k in current.items()}

    context['list_pelanggan'] = result

    return render(request, 'penyewa.html', context)

def pengguna_pemilik(request):
    context = {}

    result = cursor.execute('select * from pelanggan p join pemilik pm on p.id_pelanggan = pm.id_pelanggan', return_type = 'dict')
    for i in range(len(result)):
        current = result[i]
        result[i] = {j : k for j,k in current.items()}

    context['list_pemilik'] = result

    return render(request, 'pemilik.html', context)

def staff(request):
    context = {}

    result = cursor.execute('select * from staff s join staff_lulusan sl on s.no_staff = sl.no_staff', return_type = 'dict')
    for i in range(len(result)):
        current = result[i]
        result[i] = {j : k for j,k in current.items()}

    context['list_staff'] = result

    return render(request, 'staff.html', context)

def lukisan(request):
    context = {}

    result = cursor.execute('select * from lukisan', return_type = 'dict')
    for i in range(len(result)):
        current = result[i]
        result[i] = {j : k for j,k in current.items()}

    context['list_lukisan'] = result

    return render(request, 'lukisan.html', context)

def sewa(request):
    context = {}

    result = cursor.execute('select t.tgl_sewa, t.id_pelanggan, t.banyaknya, t.id_lukisan, tb.tenor_pilihan, t.lama_sewa, b.alamat_tujuan, b.ongkir, b.total_biaya, t.tgl_kembali, m.nama_bank, t.no_staff from transaksi_sewa t, tenor_bayar tb, biaya_sewa b, mitra_bank m where t.id_pelanggan = b.id_pelanggan and b.kode_tenor = tb.kode_tenor and m.kode_bank = tb.kode_bank', return_type = 'dict')
    for i in range(len(result)):
        current = result[i]
        result[i] = {j : k for j,k in current.items()}

    context['list_sewa'] = result

    return render(request, 'sewa.html', context)

def fee(request):
    context = {}

    result = cursor.execute('select distinct f.tgl_terima, f.id_pelanggan, l.id_lukisan, f.jumlah, l.harga, b.total_biaya from fee f, transaksi_sewa t, biaya_sewa b, lukisan l, pelanggan p, pemilik pm where f.id_pelanggan = p.id_pelanggan and p.id_pelanggan = pm.id_pelanggan and pm.id_pelanggan = l.id_pelanggan and l.id_lukisan = t.id_lukisan and t.id_pelanggan = b.id_pelanggan and t.tgl_sewa = b.tgl_sewa', return_type = 'dict')
    for i in range(len(result)):
        current = result[i]
        result[i] = {j : k for j,k in current.items()}

    context['list_fee'] = result

    return render(request, 'fee.html', context)
