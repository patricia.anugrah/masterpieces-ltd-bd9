from django import forms

class LoginForm(forms.Form):
    id_pelanggan = forms.CharField(max_length=10)
    email_pelanggan = forms.EmailField()
