from django.db import connection
from collections import namedtuple

class QueryExecuter(object):

    def execute(self, query, *args, **kwargs):
        with connection.cursor() as cursor:
            cursor.execute(query, args)
            return_type = None if 'return_type' not in kwargs else kwargs['return_type']

            if return_type == 'dict':
                return self.dictfetchall(cursor)
            if return_type == 'tuple':
                return self.namedtuplefetchall(cursor)
            
            if 'cone' in kwargs and kwargs['cone']:
                return cursor.fetchcone()
            return cursor.fetchall()

    def dictfetchall(self, cursor):
        "Return all rows from a cursor as a dict"
        columns = [col[0] for col in cursor.description]
        return [
            dict(zip(columns, row))
            for row in cursor.fetchall()
        ]

    def namedtuplefetchall(self, cursor):
        "Return all rows from a cursor as a namedtuple"
        desc = cursor.description
        nt_result = namedtuple('Result', [col[0] for col in desc])
        return [nt_result(*row) for row in cursor.fetchall()]